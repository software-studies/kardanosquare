﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KardanoSquare
{
    class EncryptionHandler
    {
        EncryptionTextHandler EncryptionTextHandler;
        MatrixHandler MatrixHandler;
        public EncryptionHandler(EncryptionTextHandler encryptionTextHandler, MatrixHandler matrixHandler)
        {
            EncryptionTextHandler = encryptionTextHandler;
            MatrixHandler = matrixHandler;
        }
        
        public string Encrypt(string plainText, int[,] stencilMatrix, char[,] textMatrix, bool[,] fillMatrix, int matrixSize)
        {
            // Аннулировать bool матрицу. Если метод Encrypt был вызван, значит, текст нужно зашифровать заново.
            MatrixHandler.restoreMatrix(fillMatrix, matrixSize);

            // Отдельный индекс для строки, куда будет вноситься зашифрованный текст 
            int textIndex = 0;

            // Матрицу нужно будет повернуть на 90 градусов 3 раза. 0 – 90 – 180 – 270; 360 – возвращается, но уже не обрабатывается
            int degree = 0;

            // Алгоритм шифрования
            while (degree < 360)
            {
                // Обрабатываем матрицу.
                for (int i = 0; i < matrixSize; i++)
                {
                    for (int j = 0; j < matrixSize; j++)
                    {
                        // Если в трафарете стоит 1, то исходный текст еще имеет символы.
                        // Второе условие необходимо проверять на тот случай, если в трафарете еще есть пустые ячейки, но весь исходный текст уже зашифрован.
                        // В противном случае будет ошибка OutOfRange для исходного текста.
                        if (stencilMatrix[i, j] == 1 && textIndex < plainText.Length)
                        {
                            // Переносим следующий символ открытого сообщения в матрицу текста.
                            textMatrix[i, j] = plainText[textIndex];
                            // Отмечаем, что эта ячейка заполнена.
                            fillMatrix[i, j] = true;
                            textIndex++;
                        }
                    }
                }
                // повернуть трафарет по часовой стрелке на 90 градусов
                MatrixHandler.TurnStencilRight(stencilMatrix, matrixSize);
                degree += 90;
            }
            // Матрица текста заполнена, необходимо считать текст по строкам.
            return EncryptionTextHandler.CreateEncryptText(fillMatrix, textMatrix, matrixSize);
        }
        public string Descrypt(string plainText, int[,] stencilMatrix, char[,] textMatrix, bool[,] fillMatrix, int matrixSize)
        {
            int degree = 360;
            plainText = "";
            // При расшифровке матрицу необходимо поворачивать против часовой. 270 – 180 – 90 – 0 градусов.
            while (degree > 0)
            {
                MatrixHandler.TurnStencilLeft(stencilMatrix, matrixSize);
                degree -= 90;
                // Обрабатываем марицу-трафарет справа налево, снизу на верх
                for (int i = matrixSize - 1; i >= 0; i--)
                {
                    for (int j = matrixSize - 1; j >= 0; j--)
                    {
                        // Если ячейка в трафарете с "дырой" и если символ в текстовой матрице введен
                        if (stencilMatrix[i, j] == 1 && fillMatrix[i, j] == true)
                        {
                            // Забрать текст из соответствующей ячейки текстовой матрицы
                            char character = textMatrix[i, j];
                            plainText = plainText.Insert(0, character.ToString());
                        }
                    }
                }
            }
            return plainText;
        }
    }
}

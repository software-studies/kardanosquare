﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace KardanoSquare
{
    class StencilViewHandler
    {
        const double gridLenght = 30;
        object container;
        public StencilViewHandler(object container)
        {
            this.container = container;
        }

        public void Draw(int size, RoutedEventHandler eventHandler)
        {
            Grid grid = (Grid)container;
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.VerticalAlignment = VerticalAlignment.Center;

            myClass[,] matr_name_button = GetNameMatrix(size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    grid.RowDefinitions.Add(new RowDefinition());
                    grid.ColumnDefinitions.Add(new ColumnDefinition());

                    Button button = new Button();
                    Grid.SetRow(button, i);
                    Grid.SetColumn(button, j);
                    button.Height = gridLenght;
                    button.Width = gridLenght;
                    button.Click += eventHandler;
                    TextBlock textBlock = new TextBlock();
                    textBlock.FontSize = 24;
                    textBlock.Padding = new Thickness(1);
                    textBlock.Text = matr_name_button[i, j].Name.ToString();
                    button.Content = textBlock.Text;
                    button.Foreground = new SolidColorBrush(Colors.Black);
                    button.Background = matr_name_button[i, j].Color;


                    grid.Children.Add(button);
                }
            }
            //((Grid)container).Children.Add(grid);
        }

        public myClass[,] GetNameMatrix(int size)
        {
            int min_size = size / 2;
            myClass[,] matr_name_button = new myClass[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matr_name_button[i, j] = new myClass();
                    matr_name_button[i, j].Name = 0;
                }
            }

            int k = 1;
            for (int i = 0; i < min_size; i++)
            {
                for (int j = 0; j < min_size; j++)
                {
                    matr_name_button[i, j].Name = k;
                    matr_name_button[i, j].Color = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#C7D0CC"));
                    k++;
                }
            }

            k = 1;
            for (int j = size - 1; j > min_size - 1; j--)
            {
                for (int i = 0; i < min_size; i++)
                {
                    matr_name_button[i, j].Name = k;
                    matr_name_button[i, j].Color = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#A5A5A5"));
                    k++;
                }
            }

            k = 1;
            for (int i = size - 1; i > min_size - 1; i--)
            {
                for (int j = size - 1; j > min_size - 1; j--)
                {
                    matr_name_button[i, j].Name = k;
                    matr_name_button[i, j].Color = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#8D917A"));
                    k++;
                }
            }

            k = 1;
            for (int j = 0; j < min_size; j++)
            {
                for (int i = size - 1; i > min_size - 1; i--)
                {
                    matr_name_button[i, j].Name = k;
                    matr_name_button[i, j].Color = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#6D6552"));
                    k++;
                }
            }

            return matr_name_button;
        }

        public void Clean()
        {
            ((Grid)container).Children.Clear();
        }


    }
    public class myClass
    {
        int _name;
        SolidColorBrush _color;/* new SolidColorBrush(Colors.DarkRed);*/

        public int Name { get => _name; set => _name = value; }
        public SolidColorBrush Color { get => _color; set => _color = value; }
    }
}

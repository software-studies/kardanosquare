﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KardanoSquare
{
    class MatrixHandler
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="matrix">Матрица, которую нужно повернуть вправо</param>
        /// <param name="size">Размер матрицы</param>
        public void TurnStencilRight(int[,] matrix, int size)
        {
            // копия матрицы-трафарета, для возврата матрицы-трафарета вправо
            int[,] stencilCopyMatrix;
            stencilCopyMatrix = new int[size, size];
            // скопировать матрицу трафарет в дополнительную матрицу
            CopyMatrix(stencilCopyMatrix, matrix, size);
            // онулить stencilMatrix
            restoreMatrix(matrix, size);
            int k = size - 1;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int digit = stencilCopyMatrix[i, j];
                    matrix[j, k] = digit;
                }
                k--;
            }
        }

        public void TurnStencilLeft(int[,] matrix, int size)
        {
            // вспомогательная матрица, чтобы повернуть трафарет влево
            int[,] stencilCopyMatrix;
            stencilCopyMatrix = new int[size, size];
            CopyMatrix(stencilCopyMatrix, matrix, size);
            // очистить stencil матрицу
            restoreMatrix(matrix, size);
            for (int i = 0; i < size; i++)
            {
                int k = size - 1;
                for (int j = 0; j < size; j++)
                {
                    int digit = stencilCopyMatrix[i, j];
                    matrix[k, i] = digit;
                    k--;
                }
            }
        }

        public void restoreMatrix(int[,] matrix, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = 0;
                }
            }
        }

        public void restoreMatrix(bool[,] matrix, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = false;
                }
            }
        }

        /// <summary>
        /// Функция производит копирование матрицы
        /// </summary>
        /// <param name="matrixA">Матрица в которую копируют</param>
        /// <param name="matrixB">Матрица из которой копируют</param>
        void CopyMatrix(int[,] matrixA, int[,] matrixB, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrixA[i, j] = matrixB[i, j];
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KardanoSquare
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        int[,] stencilMatrix;
        /// <summary>
        /// Матрица, показывающая, заполнена ли ячейка в матрице текста
        /// </summary>
        bool[,] fillMatrix;

        /// <summary>
        /// Матрица, содержащая в себе текст, заполненный через матрицу-трафарет
        /// </summary>
        char[,] textMatrix;

        /// <summary>
        /// Размер матрицы
        /// </summary>
        int matrixSize;

        /// <summary>
        /// Минимальное количество ячеек, которые необходимо выделить в матрице-трафарете
        /// </summary>
        int minSelectedCellCount = 0;

        /// <summary>
        /// Фактическое количество ячеек, которые были выделены в матрице-трафарете
        /// </summary>
        private int _practSelectedCellCount = 0;
        public int practSelectedCellCount
        {
            get => _practSelectedCellCount;
            set
            {
                _practSelectedCellCount = value;
                OnPropertyChanged(nameof(practSelectedCellCount));
            }
        }

        /// <summary>
        /// Минимальный размер матрицы
        /// </summary>
        private int _minRazmerMatr = 0;
        public int MinRazmerMatr
        {
            get => _minRazmerMatr;
            set
            {
                _minRazmerMatr = value;
                OnPropertyChanged(nameof(MinRazmerMatr));
            }
        }

        /// <summary>
        /// Открытый текст
        /// </summary>
        string plainText;

        StencilViewHandler stencilHandler;
        MatrixHandler MatrixHandler;
        EncryptionTextHandler EncryptionTextHandler;
        EncryptionHandler EncryptionHandler;


        public MainWindow()
        {
            InitializeComponent();

            this.NowSelectTextBlock.DataContext = this;
            this.MinRazmrMatr.DataContext = this;
            chengeMinRazmer_Matr();

            stencilHandler = new StencilViewHandler(stencilContainer);
            minSelectedCellCount = Int32.Parse(CellsToSelectTextBlock.Text);

            MatrixHandler = new MatrixHandler();
            EncryptionTextHandler = new EncryptionTextHandler();
            EncryptionHandler = new EncryptionHandler(EncryptionTextHandler, MatrixHandler);
        }

        private void chengeMinRazmer_Matr()
        {
            if (Int32.Parse(textLengthTextBlock.Text) > 0)
            {
                var a = (double)Int32.Parse(textLengthTextBlock.Text);
                var b = Math.Sqrt(a);
                var c = (int)Math.Ceiling(b);
                if(c%2==0)
                    MinRazmerMatr = c;
                else
                    MinRazmerMatr = c + 1;
            }
        }

        public void StencilButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int column = Grid.GetColumn(button);
            int row = Grid.GetRow(button);

            myClass[,] name_button = stencilHandler.GetNameMatrix(matrixSize);
            string name = name_button[row, column].Name.ToString();
            SolidColorBrush color = name_button[row, column].Color;

            if ((string)button.Content == name)
            {
                button.Content = "(" + name + ")";
                stencilMatrix[row, column] = 1;
                HighlightButton(button);
                practSelectedCellCount++;
            }
            else
            {
                button.Content = name_button[row, column].Name.ToString();
                stencilMatrix[row, column] = 0;
                UnhighlightButton(button, color);
                practSelectedCellCount--;
            }
        }

        private void UnhighlightButton(Button button, SolidColorBrush color)
        {
            button.Background = color;
            button.Foreground = new SolidColorBrush(Colors.Black);
        }

        // highlight button with content "1"
        public void HighlightButton(Button button)
        {
            button.Background = new SolidColorBrush(Colors.DarkRed);
            button.Foreground = new SolidColorBrush(Colors.White);
        }

        private void CreateStencilButton_Click(object sender, RoutedEventArgs e)
        {
            // проверить не пустой ли текст для шифрования
            if (plainTextBox.Text.Length != 0)
            {
                plainText = plainTextBox.Text;
                if (sizeTextBox.Text.Length != 0)
                {
                    try
                    {
                        double number;
                        number = Double.Parse(sizeTextBox.Text);
                        if (number % 2 == 0)
                        {
                            matrixSize = Convert.ToInt32(number);
                            if (matrixSize * matrixSize >= plainTextBox.Text.Length)
                            {
                                practSelectedCellCount = 0;
                                stencilHandler.Clean();
                                stencilHandler.Draw(matrixSize, StencilButton_Click);
                                stencilMatrix = new int[matrixSize, matrixSize];
                                // проверить инициализируется ли массив как false;
                                fillMatrix = new bool[matrixSize, matrixSize];
                                textMatrix = new char[matrixSize, matrixSize];
                                encryptButton.IsEnabled = true;
                            }
                            else
                            {
                                MessageBox.Show("Матрица мала для этого текста");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Введите четное число");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Введите целое число");
                    }
                }
                else
                {
                    MessageBox.Show("Введите число в поле");
                }
            }
            else
            {
                MessageBox.Show("Необходимо ввести открытый текст");
            }
        }

        private void encryptButton_Click(object sender, RoutedEventArgs e)
        {
            // Если выделено необходимое минимальное или большее количество ячеек. Чтобы в матрицу влезло все сообщение
            if (practSelectedCellCount >= minSelectedCellCount)
            {
                // тут вызов EncriptionHandler.Encrypt
                encryptedTextBlock.Text =
                    EncryptionHandler.Encrypt(plainTextBox.Text, stencilMatrix, textMatrix, fillMatrix, matrixSize);

                descryptButton.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Выделено мало ячеек на трафарете");
            }
        }

        private void plainTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textLengthTextBlock != null)
            {
                int digit = Int32.Parse(textLengthTextBlock.Text);
                // при делении округлить в большую сторону
                int mod = digit % 4;
                digit /= 4;
                if (mod > 0)
                {
                    digit++;
                }
                minSelectedCellCount = digit;
                // Количество ячеек, которые необходимо выделить
                CellsToSelectTextBlock.Text = digit.ToString();

                chengeMinRazmer_Matr();
            }
        }

        private void descryptButton_Click(object sender, RoutedEventArgs e)
        {
            descryptedTextBlock.Text =
                EncryptionHandler.Descrypt(plainText, stencilMatrix, textMatrix, fillMatrix, matrixSize);
        }

        private void Button_Click_info(object sender, RoutedEventArgs e)
        {
            string messege = "";
            messege += "Матрицу нужно заполнять таким образом, чтобы при повороте каждого его фрагмента на 90 градусов, относительно центра - ячейки не накладывались друг на друга.\n\n";
            messege += "Один из вариантов выполнения:\n\n";
            messege += "Ячейки выбирать по порядку в каждом блоке, но не следует выбирать один и тот же блок подряд.";

            MessageBox.Show(messege);
        }

        private void Button_Click_copyright(object sender, RoutedEventArgs e)
        {
            string messege = "";
            messege += "         МИНИСТЕРСТВО ТРАНСПОРТА РОССИЙСКОЙ ФЕДЕРАЦИИ\n";
            messege += "              ФЕДЕРАЛЬНОЕ ГОСУДАРСТВЕННОЕ АВТОНОМНОЕ\n";
            messege += "       ОБРАЗОВАТЕЛЬНОЕ УЧРЕЖДЕНИЕ ВЫСШЕГО ОБРАЗОВАНИЯ\n";
            messege += "              «РОССИЙСКИЙ УНИВЕРСИТЕТ ТРАНСПОРТА (МИИТ)»\n\n";
            messege += "                     «Институт пути, строительства и сооружений»\n";
            messege += "           Кафедра «Системы автоматизированного проектирования»\n\n";
            messege += "КУРСОВОЙ ПРОЕКТ  ПО ДИСЦИПЛИНЕ «ЗАЩИТА ИНФОРМАЦИИ»\n";
            messege += "НА ТЕМУ: «ПРОГРАММНАЯ РЕАЛИЗАЦИЯ АЛГОРИТМОВ ШИФРОВАНИЯ - РЕШЁТКА КАРДАНО»\n\n";
            messege += "Выполнил:\n";
            messege += "       Студент группы САП - 411»\n";
            messege += "       Чекалов И.К\n";

            messege += "\t\t\t\t© Чекалов И.К., 2021";

            MessageBox.Show(messege);
        }

        public event PropertyChangedEventHandler PropertyChanged; 
        /// <summary>
        /// Реализация интерфейса INotifyPropertyChanged
        /// </summary>
        public virtual void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }


    }
}
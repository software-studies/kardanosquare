﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KardanoSquare
{
    class EncryptionTextHandler
    {

        /// <summary>
        ///
        /// </summary>
        /// <param name="fillMatrix">Матрица bool, показывает, из каких ячеек текстовой матрицы необходимо считывать символы</param>
        /// <param name="textMatrix">Текстовая матрица</param>
        /// <param name="size">Размер матрицы</param>
        /// <returns></returns>
        public string CreateEncryptText(bool[,] fillMatrix, char[,] textMatrix, int size)
        {
            string encryptedText = "";
            int textIndex = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (fillMatrix[i, j] == true)
                    {
                        char character = textMatrix[i, j];
                        encryptedText = encryptedText.Insert(textIndex, character.ToString());
                        textIndex++;
                    }
                }
            }
            return encryptedText;
        }
    }
}
